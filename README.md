# Announcements
2023-07-15: **voidr** has been rewritten completely and is now more robust. Some addtional features were added as well.

# Content

- **void** bash script (a xbps-* convenience script)
- **voids** bash script (a xbps-src convenience script)
- **voidr** (a runit related convenience script to handle services)
- **Vpdate** bash script (a one click CLI Void updater)

Comming soon:
- **KdeVoid** (a script for a fully functional KDE Plasma experience for Void Linux base install; way way more than just installing kde5 and kde5-baseapps)

### Contribute

- Find bugs or bad code
- Assist to make the code more efficient
- Suggest commands that should also be featured in the script

# The "void" Script

Sick of all the lengthy commands in regard to XBPS on Void Linux? The **void** bash script makes XBPS more convenient by just being a layer above the most commom xbps commands.

### Dependencies
The script is dependent on **xtools**, which can easily be installed by `xbps-install -S xtools`. Otherwise, just delete the call for the function **restart** in the script: case **update** and case **restart**.

### How to use

You just copy the script to your prefered folder, make it executabel (`chmod +x /your/path/to/void`) and add the location to your \$PATH (e.g. in your `~/.bashrc`: `PATH=$PATH:/your/path/to/void`; don't forget to run `source ~/.bashrc` afterwards).

Now you're able to run commands like:

```
$ void update ...
$ void install ...
$ void search ...
$ void remove ...
$ void ...
```

If you're not happy with the name "void", you can easily rename the script and use your prefered name instead. The help dialog will ajust acordingly.

### Features

```
Valid Input: $ void install <pkg1> <pkg2> ...
             $ void remove <pkg1> <pkg2> ...
             $ void search <pkg> (searches for packages in your remote repos)
                           -i <pkg> (searchs only for installed packages)
                           -n <pkg> (searches only for non-installed packages)
                           -a 32 <pkg> (searches only for 32 bit packages)
                           -a 64 <pkg> (searches only for 64 bit packages)
                           [possible combinations: -ia 32 <pkg>; -ia 64 <pkg>; -na 32 <pkg>; -na 64 <pkg>]
             $ void info <pkg> (shows information about a package)
             $ void find <pkg> (finds all folders a package is located in)
             $ void update (checks if xbps is up to date, updates packages, clears cache, removes orphans, restarts updated services [dependent on xtools])
             $ void just-update (updates packages)
             $ void local (shows all local packages)
             $ void cache (clears cache)
             $ void orphans (removes orphans)
             $ void restart (restarts updated services; dependend on xtools)
             $ void kernels (removes all old kernels)
             $ void repo (lists installed repos)
             $ void sync (syncs repos)
             $ void help (this help dialog)
             $ void (this help dialog)
```

# The "voids" Script

**Voids** is a bash script that makes your life easier when it comes to xbps-src.

### How to use

IMPORTANT: You need to have void-packages downloaded and bootstraped.\
Make sure void-packages is lokated somewhere in your /home/USER/.../.../... folder. The script will search for it there. TIP: If it's located directly in /home/USER/ the script will run much faster.\
See the **How to use** section of the **void** script.

Now you're able to run commands like:

```
$ voids install ... (compiles the package and installs it)
$ voids search ...
$ voids remove ...
$ voids go ...
$ voids ...
```

If you're not happy with the name "voids", you can easily rename the script and use your prefered name instead. The help dialog will ajust acordingly.

### Features

```
Valid Input: $ voids install <pkg> (updates git-repo, builds and installs package, removes the xbps-file from binpkgs/*)
             $ voids search <pkg> (updates the git-repo and searches for a package)
             $ voids remove <pkg> (removes package)
             $ voids build <pkg> (updates git-repo and builds the package)
             $ voids install-built <pkg> (installs built packages from /binpkgs/*)
             $ voids remove-built <pkg> (removes the xbps-file from binpkgs/*)
             $ voids go <pkg> (moves you to the srcpkg folder of the package)
             $ voids update (updates git-repo)
             $ voids help (this help dialog)
             $ voids (this help dialog)
```

# The "voidr" Script

**Voidr** is a bash script that handles runit related stuff in a consistent and easy way. All commands are related to **sv**, `/etc/sv` and `/var/service`.\
NOTE: The **voidr** script handles multiple services at once and is able to process wildcards (e.g. `voidr enable Net* virt* cup*`)

### How to use

To add the script to your PATH, see the "How to use" section of the **void** script.

Now you're able to run commands like:

```
$ voidr enable ...
$ voidr disable ...
$ voidr start ...
$ voidr stop ...
$ voidr ...
```
If you're not happy with the name "voids", you can easily rename the script and use your prefered name instead. The help dialog will ajust acordingly.

### Features
```
Usage:

voidr [operation] <service_1> <service_2> ...

voidr [operation_1] [operation_2]

         Wildcards are processed only at the end of an argument and 
         only for arguments of 3 or more characters.
         e.g. valid input:
         $ voidr enable cup* virt*
         e.g. invalid input:
         $ voidr enable * d* ne* e*in *ger
         Invalid arguments will be ignored and excluded from further 
         processing.
         
         Generally: The actual service name is used for 
         processing (sourced from /etc/sv/ and/or /var/service/ 
         resp.) to protect the script from processing uncontrolled 
         data. This applies to ALL operations provided by the voidr 
         script.
         
Operations:

enable <service_1> <service_2> ...

         alternatively: link
         ...enables services and starts them in one go (based on 
         ln -s /etc/sv/<service> /var/service and sv 
         up <service>). If a service does not exist in 
         /etc/sv/, the service will be skipped. If a service fails 
         to start, the starting process is repeated once before the 
         script stops attempting.
         
         
disable <service_1> <service_2> ...

         alternatively: remove
         ...disables services (based on rm -R 
         /var/service/<service>). If a service does not exist 
         in /var/service/, the service will be skipped.
         
         
start <service_1> <service_2> ...

         alternatively: up
         ...starts services (based on sv up <service>). 
         If a service fails to start, the process is repeated once 
         before the service is skipped.
         
         
stop <service_1> <service_2> ...

         alternatively: down
         ...stops services (based on sv down <service>).
         
         
restart <service_1> <service_2> ...

         ...restarts services (based on sv restart 
         <service>).
         
         
restart inactive
         alternatively:restart down | restart 
         stopped | restart stop]
         ...starts all services that are down. It greps services 
         labled "down" (based on sv status) and starts 
         all of them (based on sv start <service_down_1> 
         ...).
         
         
restart failed
         alternatively: restart fail
         ...starts all services that failed to start. It greps 
         services labled "fail" (based on sv status) and 
         starts all of them (based on sv start <service_down_1> 
         ...).
         
         
status <service_1> <service_2> ...

         ...displays a simplified list of the chosen services that 
         are running, down, or failed to run (based on sv 
         status)
         
         
status
         alternatively: status all
         ...displays a simplified list of all enabled services 
         (running, down, and failed; based on sv status).
         
         
status inactive
         alternatively: status down | status 
         stopped | status stop
         ...displays a simplified list of all services that are down 
         (based on sv status)
         
         
status failed
         alternatively: status fail
         ...displays a simplified list of all services that failed 
         to start (based on sv status)
         
         
list
         ...lists all services in /etc/sv/ and /var/service/ side by 
         side comparitively (enabled, not enabled).
         
         
path
         ...simply displays the content of both directories /etc/sv/ 
         and /var/service/.
         
         
help
         alternatively: <no_argument>
         ...displays this usage dialog.

```

# The "Vpdate"-er

**Vpdate** is a one click CLI updater for Void Linux (bash). It does NOT rely on the void script described above. You can use it as a backend for a desktop shortcut, a panel icon, or a menu entry inside your app launcher. I added a icon, too, if you wish to use this as well.\
NOTE: Currently, Flatpaks are updated, too. If you don't need that just delete the flatpak part in the script.\
NOTE2: Currently, the starter script calls kde konsole. At this stage of the script you have to change it manually if needed. This will be fixed in the future.

### How to use

Vpdate consists of two scripts, one that will perform the updates of your xbps manager. Only the "**VpdateCLI**" script needs to be in your \$PATH (see above, how to do that). The "**VpdateCLI_Starter**" is the one you need to refer to when adding it to your prefered starter (in my case the KDE Plasma app launcher; it just says update).

### What the Vpdate does

- checks if xbps is up to date and updates it when necessary
- starts the regular update
- clears the cache
- removes the orphans
